<?php
class tren
{
    public $nume;
    public $numarVagoane;
    public $tip;

    public function __construct($nume)
    {
        $this->nume = $nume;
        $this->numarVagoane = 10;
        $this->tip = ('calatori');
    }
    public function setNume($nume)
    {
        $this->nume = $nume;
    }
    public function getNume()
    {
        return $nume->nume;
    }
    public function setNumarVagoane($numarVagoane)
    {
        $this->numarVagoane = $numarVagoane;
    }
    public function getNumarVagoane()
    {
        return $numarVagoane->numarVagoane;
    }
    public function setTip($tip)
    {
        $this->tip = $tip;
    }
    public function getTip()
    {
        return $tip->tip;
    }
    public function descriereTren()
    {
        return "Trenul " . $this->nume . " are " . $this->numarVagoane . " vagoane si este de tipul " . $this->tip;
    }
}

?>
