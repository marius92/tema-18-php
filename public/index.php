<?php
require('../class/vehicul.php');
require('../class/tren.php');
require('../class/avion.php');
require('../class/masina.php');
require('../class/bicicleta.php');



$bicicleta = new bicicleta('BMX');
echo $bicicleta->getModel() . ' are ' . $bicicleta->getRoti() . ' roti.<br>';

echo ("<br>");

$masina = new masina('Skoda');
$masina->setModel('Skoda');
echo $masina->descriereMasina();

echo ("<br>");

$tren = new tren('CFR');
$tren->setNumarVagoane(15);
$tren->setNume('CFR');

echo $tren->descriereTren();

echo ("<br>");

$avion = new avion('Airliner');
$avion->setNume('Concorde');
$avion->setTip('de calatori');
echo $avion->descriereAvion();

?>
